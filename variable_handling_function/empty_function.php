<?php

/* 
Learning Empty function.
 * The empty function is used to check whether a variable is empty or not
 * Return value

FALSE if var_name has a non-empty and non-zero value.

Value Type : Boolean

List of empty things :

"0" (0 as a string)
0 (0 as an integer)
"" (an empty string)
NULL
FALSE
"" (an empty string)
array() (an empty array)
$var_name; (a variable declared but without a value in a class)
 */

$var1 = "";

if(empty($var1))
{
    echo $var1." is empty";
}
else
{
    echo $var1." is not empty";
}

var_dump(empty($var1));