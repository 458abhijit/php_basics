<?php

/* 
 * The unserialize() converts to actual data from serialized data.
 * Return value

Converted value. The value can be a boolean, array, float, integer or string.
 */

$serialized_data = serialize(array("One","Four","Seven"));

echo $serialized_data. "<br>";

$unserial = unserialize($serialized_data);

var_dump($unserial);