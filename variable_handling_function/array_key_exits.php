<?php

/* array_key_exists function
 * array_key_exists — Checks if the given key or index exists in the array

Parameters 

key
Value to check.

array
An array with keys to check.

Return Values ¶

Returns TRUE on success or FALSE on failure.
 */


$search_array = array('first' => 1, 'second' => 4);

if (array_key_exists('first', $search_array)) {
    echo "The 'first' element is in the array";
}
