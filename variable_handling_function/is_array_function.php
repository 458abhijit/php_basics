<?php

/* 
 Learning isset function.
     * The is_array function is used to find whether a variable is an array or not  
 * Return value

TRUE if var is an array, FALSE otherwise.

Value Type : Boolean.
 */

$array_name = array();

if(is_array($array_name))
{
    echo "This is an array";
}
else
{
    echo "This is not an array";
}