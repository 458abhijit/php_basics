<?php

/* 
    Learning isset function.
     * The isset function is used to check whether a variable is set or not  
 * Return value

TRUE if variable (variable1,variable2..) exists and has value not equal to NULL, FALSE otherwise.

Value Type : Boolean.. 
 */

$var = null;

var_dump(isset($var));

var_dump(empty($var));