<?php

/* 
 * The unset() function destroys a given variable.
 */

$xyz='w3resource.com';  
echo 'Before using unset() the value of $xys is : '. $xyz.'<br>';  
unset($xyz);  
echo 'After using unset() the value of $xys is : '. $xyz; 