<?php

/* 
 * The is_object () function is used to find whether a variable is object or not.
 * Syntax

is_object(var_name)
 * 
 * Return value

TRUE if var_name is an object, FALSE otherwise.

Value Type : Boolean.
 */

function get_subjects($obj_name)  
{  
if(!is_object($obj_name))  
{  
return(false);  
}  
return($obj_name->subjects);  
}  
$obj_name = new stdClass;  
$obj_name->subjects = Array('Physics', 'Chemistry', 'Mathematics');  
var_dump(get_subjects(NULL));  
var_dump(get_subjects($obj_name));  